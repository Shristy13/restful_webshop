package restful.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import restful.model.Product;

import java.lang.reflect.Type;

public class ProductDeserializer implements JsonDeserializer<Product> {
    @Override
    public Product deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        Product product = new Product();
        product.setId(jsonElement.getAsJsonObject().get("id").getAsInt());
        product.setName(jsonElement.getAsJsonObject().get("name").getAsString());
        product.setValue(jsonElement.getAsJsonObject().get("value").getAsDouble());
        product.setPlatform(jsonElement.getAsJsonObject().get("platform").getAsString());
        product.setCountry(jsonElement.getAsJsonObject().get("country").getAsString());

        return product;
    }
}
