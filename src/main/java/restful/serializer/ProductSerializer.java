package restful.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import restful.model.Product;

import java.lang.reflect.Type;

public class ProductSerializer implements JsonSerializer<Product> {
    @Override
    public JsonElement serialize(Product src, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject =  new JsonObject();
        jsonObject.addProperty("id",src.getId());
        jsonObject.addProperty("name",src.getName());
        jsonObject.addProperty("value",src.getValue());
        jsonObject.addProperty("plaform",src.getPlatform());
        jsonObject.addProperty("country",src.getCountry());
        jsonObject.addProperty("brand_id",src.getBrand().getId());

        return jsonObject;

    }
}
