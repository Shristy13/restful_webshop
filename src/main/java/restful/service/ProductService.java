package restful.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import restful.dao.BrandDao;
import restful.dao.ProductDao;
import restful.model.Brand;
import restful.model.Product;
import restful.serializer.ProductSerializer;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

@Path   ("brands/{brand_id}/prod")
@Produces(MediaType.APPLICATION_JSON)
public class ProductService {
    private final GsonBuilder builder = new GsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapter(Product.class , new ProductSerializer());
    public Gson gson = builder.create();

    private final ProductDao productDao = new ProductDao();
    private final BrandDao brandDao  = new BrandDao();

    @GET
    public Response list(@PathParam("brand_id") int brandId) {
        Brand brand = brandDao.get(brandId);

        if (brand !=null) {
            String respose = gson.toJson(brand.getProducts());
            return Response.ok(respose).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("brand_id") int brandId , @PathParam("id") int id) {
        Brand brand = brandDao.get(brandId);
        Product product = productDao.get(id);

        if (brand != null && product != null && Objects.equals(product.getBrand().getId(), brand.getId())) {
            String response = gson.toJson(product);
            return Response.ok(response).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(@PathParam("brand_id") int brandId , String json) {
        Brand brand = brandDao.get(brandId);
        Product product = gson.fromJson(json , Product.class);
        product.setBrand(brand);

        if(productDao.create(product)) {
            return Response.status(Response.Status.OK).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
